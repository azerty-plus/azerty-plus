include .env

build:
	docker build -t $(IMAGE) .

run:
	docker run -dp $(PORT):8080 --name $(CONTENEUR) $(IMAGE)

kill:
	docker stop $(CONTENEUR)
	docker rm $(CONTENEUR)

clean: kill
	docker rmi $(IMAGE)
