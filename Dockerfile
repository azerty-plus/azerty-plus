FROM node:current-alpine AS builder
WORKDIR /app
COPY package.json ./
RUN npm install
COPY ./ ./
RUN npm run build

FROM busybox:musl
RUN adduser -D static
USER static
WORKDIR /home/static
COPY --from=builder /app/dist .
CMD ["busybox", "httpd", "-f", "-v", "-p", "8080"]
