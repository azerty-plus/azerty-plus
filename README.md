# Clavier Azerty +

Projet permettant de présenter une configuration clavier dérivée d’Azerty et améliorée pour fournir plus de possibilités (symboles typographiques, mathématiques et phonétiques principalement).

## Installation

Ce projet repose sur un environnement <cite>Node.js</cite> et nécessite un gestionnaire de paquets <cite>JavaScript</cite>.

## Démarrer

### Développement

Commencez par installer les dépendances avec `npm install`, `pnpm install` ou `yarn install`.

Dans un environnement de développement, démarrez le serveur avec une des commandes suivantes via :
* en développement (avec HMR) : `npm run dev`
* en recette : `npm run build && npm run preview`

Et rendez‐vous sur votre navigateur à l’adresse indiquée dans le terminal ou vérifiez le statut avec `curl -I localhost:<PORT>`. Vous pouvez changer le port dans le fichier `.env`.

### Production

Pour créer un conteneur, utilisez l’utilitaire <cite>Make</cite> :

* `make build run`, ou `make kill build run` si le conteneur existe déjà

Vous pouvez configurer les paramètres de l’image et du conteneur dans le fichier `.env` ou les écraser directement en ligne de commande en les ajoutant à la fin (par exemple `make run PORT=8080`).

Nettoyez votre environnement de travail avec `make clean`. Il vous incombe de supprimer les images intermédiaires gardées en cache, par exemple via `docker image prune`.

## Support

N’hésitez pas à ouvrir une <i>issue</i> pour obtenir de l’aide, soumettre des suggestions ou signaler des bogues. Vous pouvez me contacter à l’adresse florian.monfort@proton.me.

## Feuille de route

* Ajouter des contrôles unitaires avec <cite>Vitest</cite>
* Ajouter des contrôles de bout en bout avec <cite>Cypress</cite>
* Ajouter de l’intégration continue

## Contribuer

Vous pouvez librement proposer des changements dans le code en publiant des demandes de fusion.

Vous devez valider votre code avec la configuration <cite>TypeScript</cite> fournie.

## Auteurs et contributeurs

Principalement écrit par Florian Monfort.

## License

Cette application est sous licence GPLv3.
