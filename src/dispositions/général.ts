import type { Touche } from "@/dispositions/index.ts"
import PositionTouche from "@/énumérations/PositionTouche.ts"

export default [
	[
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "3 en exposant",
				valeur: "³",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "exposant de l’abréviation féminine en espagnol, italien et portugais",
				valeur: "ª",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "2 en exposant",
				valeur: "²",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "exposant de l’abréviation masculine en espagnol, italien et portugais",
				valeur: "º",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "1",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole du discriminant",
				valeur: "∆",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "esperluette",
				valeur: "&",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "diacritique accent aigu",
				valeur: "◌́",
				action: "accentAigu",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "2",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "E accent aigu",
				valeur: "É",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "e accent aigu",
				valeur: "é",
			},
			[PositionTouche.BasDroite]: {
				nouveau: false,
				titre: "diacritique tilde",
				valeur: "◌̃",
				action: "tilde",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "3",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole « Il existe »",
				valeur: "∃",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "guillemet droit double",
				valeur: "\"",
			},
			[PositionTouche.BasDroite]: {
				nouveau: false,
				titre: "croisillon",
				valeur: "#",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "4",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole « Pour tout »",
				valeur: "∀",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "guillemet droit simple ou apostrophe droite",
				valeur: "'",
			},
			[PositionTouche.BasDroite]: {
				nouveau: false,
				titre: "accolade ouvrante",
				valeur: "{",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "5",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole « Il n’existe pas »",
				valeur: "∄",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "parenthèse ouvrante",
				valeur: "(",
			},
			[PositionTouche.BasDroite]: {
				nouveau: false,
				titre: "crochet ouvrant",
				valeur: "[",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "6",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "diacritique barre oblique",
				valeur: "/",
				action: "barreOblique",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "tiret",
				valeur: "-",
			},
			[PositionTouche.BasDroite]: {
				nouveau: false,
				titre: "barre verticale",
				valeur: "|",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "7",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "E accent grave",
				valeur: "È",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "e accent grave",
				valeur: "è",
			},
			[PositionTouche.BasDroite]: {
				nouveau: false,
				titre: "diacritique accent grave",
				valeur: "◌̀",
				action: "accentGrave",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "8",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole de l’infini (lemniscate)",
				valeur: "∞",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "tiret bas",
				valeur: "_",
			},
			[PositionTouche.BasDroite]: {
				nouveau: false,
				titre: "barre oblique inversée",
				valeur: "\\",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "9",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "C cédille",
				valeur: "Ç",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "c cédille",
				valeur: "ç",
			},
			[PositionTouche.BasDroite]: {
				nouveau: false,
				titre: "lettre accent circonflexe",
				valeur: "^",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "0",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "A accent grave",
				valeur: "À",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "a accent grave",
				valeur: "à",
			},
			[PositionTouche.BasDroite]: {
				nouveau: false,
				titre: "arobase",
				valeur: "@",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "symbole du degré",
				valeur: "°",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "plus ou moins",
				valeur: "±",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "parenthèse fermante",
				valeur: ")",
			},
			[PositionTouche.BasDroite]: {
				nouveau: false,
				titre: "crochet fermant",
				valeur: "]",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "symbole de l’addition (plus)",
				valeur: "+",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole de l’inégalité",
				valeur: "≠",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "symbole de l’égalité",
				valeur: "=",
			},
			[PositionTouche.BasDroite]: {
				nouveau: false,
				titre: "accolade fermante",
				valeur: "}",
			},
		},
		{
			valeur: "Retour en arrière",
		},
	],
	[
		{
			valeur: "Tabulation",
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "A",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "ligature a dans l’e capitale",
				valeur: "Æ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "a",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "ligature a dans l’e bas‐de‐casse",
				valeur: "æ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "Z",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "ligature ſs ou ſz capitale (eszett)",
				valeur: "ẞ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "z",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "ligature ſs ou ſz bas‐de‐casse (eszett)",
				valeur: "ß",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "E",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "environ",
				valeur: "℮",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "e",
			},
			[PositionTouche.BasDroite]: {
				nouveau: false,
				titre: "symbole monétaire de l’euro",
				valeur: "€",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "R",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "double prime, mesure d’angle, symbole du pouce et indicateur des secondes",
				valeur: "″",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "r",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "prime, mesure d’angle, symbole du pied et indicateur des minutes",
				valeur: "′",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "T",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "tiret cadratin ou tiret long",
				valeur: "—",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "t",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "tiret demi‐cadratin ou tiret moyen",
				valeur: "–",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "Y",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "trait d’union insécable",
				valeur: "‑",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "y",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "trait d’union",
				valeur: "‐",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "U",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "U brève",
				valeur: "Ŭ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "u",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "u brève",
				valeur: "ŭ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "I",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "obèle",
				valeur: "†",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "i",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "flèche orientée à gauche et à droite",
				valeur: "↔",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "O",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "ligature o dans l’e capitale",
				valeur: "Œ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "o",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "ligature o dans l’e bas‐de‐casse",
				valeur: "œ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "P",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "p",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "symboles phonétiques",
				valeur: "ɸ",
				action: "fonetik",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "diacritique tréma",
				valeur: "◌̈",
				action: "tréma",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "diacritique macron",
				valeur: "◌̄",
				action: "macron",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "diacritique accent circonflexe",
				valeur: "◌̂",
				action: "accentCirconflexe",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "diacritique cédille",
				valeur: "◌̧",
				action: "cédille",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "symbole monétaire de la livre sterling",
				valeur: "£",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "diacritique brève",
				valeur: "◌̆",
				action: "brève",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "symbole monétaire du dollar et du peso",
				valeur: "$",
			},
			[PositionTouche.BasDroite]: {
				nouveau: false,
				titre: "symbole monétaire par défaut",
				valeur: "¤",
			},
		},
		{
			valeur: "Entrée",
			rowspan: 2,
		},
	],
	[
		{
			action: "majuscules",
			toucheRémanente: true,
			valeur: "Verrouiller capitales",
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "Q",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole du produit",
				valeur: "∏",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "q",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "symbole de la somme",
				valeur: "∑",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "S",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole de l’ensemble vide",
				valeur: "∅",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "s",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "s long bas‐de‐casse",
				valeur: "ſ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				valeur: "D",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "guillemet à double apostrophe culbuté en chef",
				valeur: "“",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "d",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "guillemet à double apostrophe en chef",
				valeur: "”",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "F",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "guillemet à simple apostrophe culbuté en chef",
				valeur: "‘",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "f",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "guillemet à simple apostrophe en chef (apostrophe courbée)",
				valeur: "’",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "G",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "guillemet à simple chevron gauche",
				valeur: "‹",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "g",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "guillemet à double chevron gauche",
				valeur: "«",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "H",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "guillemet à simple chevron droit",
				valeur: "›",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "h",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "guillemet à double chevron droit",
				valeur: "»",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "J",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "lettre apostrophe culbutée",
				valeur: "ʽ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "j",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "lettre apostrophe",
				valeur: "ʼ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "K",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "flèche orientée à gauche",
				valeur: "←",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "k",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "flèche orientée à droite",
				valeur: "→",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "L",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "points de suspensions médians verticaux",
				valeur: "⋮",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "l",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "points de suspensions médians horizontaux",
				valeur: "⋯",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "M",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "tiret numérique",
				valeur: "‒",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "m",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "symbole de la soustraction (moins)",
				valeur: "−",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "pour cent",
				valeur: "%",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "pour dix mille",
				valeur: "‱",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "u accent grave",
				valeur: "ù",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "pour mille",
				valeur: "‰",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "caractères grecs",
				valeur: "µ",
				action: "grec",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole de l’intégrale",
				valeur: "∫",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "astérisque",
				valeur: "*",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "symbole de la multiplication (croix)",
				valeur: "×",
			},
		},
	],
	[
		{
			action: "majuscules",
			valeur: "Capitales",
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "chevron droit",
				valeur: ">",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "supériorité ou égalité",
				valeur: "≥",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "chevron gauche",
				valeur: "<",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "infériorité ou égalité",
				valeur: "≤",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "W",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole d’inclusion",
				valeur: "⊂",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "w",
			},
			[PositionTouche.BasDroite]: {
				nouveau: false,
				titre: "symbole d’appartenance",
				valeur: "∈",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "X",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole de disjonction inclusive",
				valeur: "∨",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "x",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "symbole de conjonction",
				valeur: "∧",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "C",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole de disjonction exclusive",
				valeur: "⊕",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "c",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "symbole de négation",
				valeur: "¬",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "V",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole d’implication",
				valeur: "⇒",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "v",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "symbole d’équivalence",
				valeur: "⇔",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "B",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole d’intersection",
				valeur: "⋂",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "b",
			},
			[PositionTouche.BasDroite]: {
				nouveau: false,
				titre: "symbole d’union",
				valeur: "⋃",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				valeur: "N",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "n",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "point d’interrogation",
				valeur: "?",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "point d’interrogation inversé",
				valeur: "⸮",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "virgule",
				valeur: ",",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "point d’exclarrogation",
				valeur: "‽",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "point",
				valeur: ".",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "point médian",
				valeur: "·",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "point‐virgule",
				valeur: ";",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "points de suspension",
				valeur: "…",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "barre oblique",
				valeur: "/",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole de la fonction racine",
				valeur: "√",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "double point",
				valeur: ":",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "symbole de la division (obélus)",
				valeur: "÷",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "symbole typographique du paragraphe",
				valeur: "§",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "point d’exclamation interrogative",
				valeur: "⁉",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "point d’exclamation",
				valeur: "!",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "point d’interrogation exclamative",
				valeur: "⁈",
			},
		},
		{
			action: "majuscules",
			valeur: "Capitales",
			colspan: 2,
		},
	],
	[
		{
			valeur: "Contrôle",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Raccourci système",
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				valeur: "Espace insécable",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				valeur: "Espace fine insécable",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				valeur: "Espace",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				valeur: "Espace fine",
			},
			colspan: 7,
		},
		{
			action: "alternatives",
			valeur: "Alternative",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Menu contextuel",
		},
		{
			valeur: "Contrôle",
		},
	],
] as Array<Array<Touche>>
