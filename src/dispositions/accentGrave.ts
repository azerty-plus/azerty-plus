import type { Touche } from "@/dispositions/index.ts"
import PositionTouche from "@/énumérations/PositionTouche.ts"

export default [
	[
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{
			[PositionTouche.BasDroite]: {
				nouveau: false,
				titre: "diacritique accent grave",
				valeur: "◌̀",
				action: "général",
			},
		},
		{},
		{},
		{},
		{},
		{},
		{
			valeur: "Retour en arrière",
		},
	],
	[
		{
			valeur: "Tabulation",
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "A accent grave",
				valeur: "À",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "a accent grave",
				valeur: "à",
			},
		},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "E accent grave",
				valeur: "È",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "e accent grave",
				valeur: "è",
			},
		},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "Y accent grave",
				valeur: "Ỳ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "y accent grave",
				valeur: "ỳ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "U accent grave",
				valeur: "Ù",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "u accent grave",
				valeur: "ù",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "I accent grave",
				valeur: "Ì",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "i accent grave",
				valeur: "ì",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "O accent grave",
				valeur: "Ò",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "o accent grave",
				valeur: "ò",
			},
		},
		{},
		{},
		{},
		{
			valeur: "Entrée",
			rowspan: 2,
		},
	],
	[
		{
			action: "majuscules",
			toucheRémanente: true,
			valeur: "Verrouiller capitales",
		},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
	],
	[
		{
			action: "majuscules",
			valeur: "Capitales",
		},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{
			action: "majuscules",
			valeur: "Capitales",
			colspan: 2,
		},
	],
	[
		{
			valeur: "Contrôle",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Raccourci système",
		},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "lettre accent grave",
				valeur: "`",
			},
			colspan: 7,
		},
		{
			action: "alternatives",
			valeur: "Alternative",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Menu contextuel",
		},
		{
			valeur: "Contrôle",
		},
	],
] as Array<Array<Touche>>
