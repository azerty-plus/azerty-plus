import type { Touche } from "@/dispositions/index.ts"
import PositionTouche from "@/énumérations/PositionTouche.ts"

export default [
	[
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{
			valeur: "Retour en arrière",
		},
	],
	[
		{
			valeur: "Tabulation",
		},
		{},
		{},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "T cédille",
				valeur: "Ţ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "t cédille",
				valeur: "ţ",
			},
		},
		{},
		{},
		{},
		{},
		{},
		{
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "diacritique cédille",
				valeur: "◌̧",
				action: "général",
			},
		},
		{},
		{
			valeur: "Entrée",
			rowspan: 2,
		},
	],
	[
		{
			action: "majuscules",
			toucheRémanente: true,
			valeur: "Verrouiller capitales",
		},
		{},
		{},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "G cédille",
				valeur: "Ģ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "g cédille",
				valeur: "ģ",
			},
		},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
	],
	[
		{
			action: "majuscules",
			valeur: "Capitales",
		},
		{},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "C cédille",
				valeur: "Ç",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "c cédille",
				valeur: "ç",
			},
		},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{
			action: "majuscules",
			valeur: "Capitales",
			colspan: 2,
		},
	],
	[
		{
			valeur: "Contrôle",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Raccourci système",
		},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "lettre cédille",
				valeur: "¸",
			},
			colspan: 7,
		},
		{
			action: "alternatives",
			valeur: "Alternative",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Menu contextuel",
		},
		{
			valeur: "Contrôle",
		},
	],
] as Array<Array<Touche>>
