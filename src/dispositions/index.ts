import PositionTouche from "@/énumérations/PositionTouche.ts"

export type CaseCaractère = {
	nouveau: boolean
	titre?: string
	valeur: string
	action?: string
}

export type ToucheBouton = {
	action: "majuscules" | "alternatives"
	toucheRémanente?: true
	valeur: string
	colspan?: number
	rowspan?: number
}

export type ToucheCaractère = {
	[PositionTouche.HautGauche]?: CaseCaractère
	[PositionTouche.HautDroite]?: CaseCaractère
	[PositionTouche.BasGauche]?: CaseCaractère
	[PositionTouche.BasDroite]?: CaseCaractère
	colspan?: number
	rowspan?: number
}

export type ToucheNaturelle = {
	valeur: string
	colspan?: number
	rowspan?: number
}

export type Touche = ToucheBouton | ToucheCaractère | ToucheNaturelle
