import type { Touche } from "@/dispositions/index.ts"
import PositionTouche from "@/énumérations/PositionTouche.ts"

export default [
	[
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{
			valeur: "Retour en arrière",
		},
	],
	[
		{
			valeur: "Tabulation",
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "alpha capital",
				valeur: "Α",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "alpha bas‐de‐casse",
				valeur: "α",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "zêta capital",
				valeur: "Ζ",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "cho capital (lettre bactrienne)",
				valeur: "Ϸ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "zêta bas‐de‐casse",
				valeur: "ζ",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "cho bas‐de‐casse (lettre bactrienne)",
				valeur: "ϸ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "epsilon capital",
				valeur: "Ε",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "epsilon bas‐de‐casse (variante dite lunaire) renversé",
				valeur: "϶",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "epsilon bas‐de‐casse",
				valeur: "ε",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "epsilon bas‐de‐casse (variante dite lunaire)",
				valeur: "ϵ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "rhô capital",
				valeur: "Ρ",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "rhô bas‐de‐casse barré",
				valeur: "ϼ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "rhô bas‐de‐casse",
				valeur: "ρ",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "rhô bas‐de‐casse (variante)",
				valeur: "ϱ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "thêta capital",
				valeur: "Θ",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole thêta",
				valeur: "ϴ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "thêta bas‐de‐casse",
				valeur: "θ",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "thêta bas‐de‐casse (variante manuscrite)",
				valeur: "ϑ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "tau capital",
				valeur: "Τ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "tau bas‐de‐casse",
				valeur: "τ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "upsilon capital",
				valeur: "Υ",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "upsilon capital (variante)",
				valeur: "ϒ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "upsilon bas‐de‐casse",
				valeur: "υ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "iota capital",
				valeur: "Ι",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "iota bas‐de‐casse",
				valeur: "ι",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "omicron capital",
				valeur: "Ο",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "omicron bas‐de‐casse",
				valeur: "ο",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "pi capital",
				valeur: "Π",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "pi bas‐de‐casse",
				valeur: "π",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "pi bas‐de‐casse (variante archaïque)",
				valeur: "ϖ",
			},
		},
		{},
		{},
		{
			valeur: "Entrée",
			rowspan: 2,
		},
	],
	[
		{
			action: "majuscules",
			toucheRémanente: true,
			valeur: "Verrouiller capitales",
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "oméga capital",
				valeur: "Ω",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "sigma capital (variante dite lunaire)",
				valeur: "Ϲ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "oméga bas‐de‐casse",
				valeur: "ω",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "sigma bas‐de‐casse (variante dite lunaire)",
				valeur: "ϲ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "sigma capital",
				valeur: "Σ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "sigma bas‐de‐casse",
				valeur: "σ",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "sigma bas‐de‐casse (variante)",
				valeur: "ς",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "delta capital",
				valeur: "Δ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "delta bas‐de‐casse",
				valeur: "δ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "phi capital",
				valeur: "Φ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "phi bas‐de‐casse",
				valeur: "φ",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "phi bas‐de‐casse (variante)",
				valeur: "ϕ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "gamma capital",
				valeur: "Γ",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "hêta capital (lettre archaïque)",
				valeur: "Ͱ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "gamma bas‐de‐casse",
				valeur: "γ",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "hêta bas‐de‐casse (lettre archaïque)",
				valeur: "ͱ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "êta capital",
				valeur: "Η",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "êta bas‐de‐casse",
				valeur: "η",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "yot capital (lettre étrangère)",
				valeur: "Ϳ",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "ligature capitale du mot kai",
				valeur: "Ϗ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "yot bas‐de‐casse (lettre étrangère)",
				valeur: "ϳ",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "ligature bas‐de‐casse du mot kai",
				valeur: "ϗ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "kappa capital",
				valeur: "Κ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "kappa bas‐de‐casse",
				valeur: "κ",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "kappa bas‐de‐casse (variante manuscrite)",
				valeur: "ϰ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "lambda capital",
				valeur: "Λ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "lambda bas‐de‐casse",
				valeur: "λ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "mu capital",
				valeur: "Μ",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "san capital (lettre archaïque)",
				valeur: "Ϻ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "mu bas‐de‐casse",
				valeur: "μ",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "san bas‐de‐casse (lettre archaïque)",
				valeur: "ϻ",
			},
		},
		{
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "caractères grecs",
				valeur: "µ",
				action: "général",
			},
		},
	],
	[
		{
			action: "majuscules",
			valeur: "Capitales",
		},
		{
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "psi capital",
				valeur: "Ψ",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "stigma capital (ligature archaïque)",
				valeur: "Ϛ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "psi bas‐de‐casse",
				valeur: "ψ",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "stigma bas‐de‐casse (ligature archaïque)",
				valeur: "ϛ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "khi capital",
				valeur: "Χ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "khi bas‐de‐casse",
				valeur: "χ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "ksi capital",
				valeur: "Ξ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "ksi bas‐de‐casse",
				valeur: "ξ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "digamma capital (lettre archaïque)",
				valeur: "Ϝ",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "digamma capital (variante pamphylienne)",
				valeur: "Ͷ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "digamma bas‐de‐casse (lettre archaïque)",
				valeur: "ϝ",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "digamma bas‐de‐casse (variante pamphylienne)",
				valeur: "ͷ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "bêta capital",
				valeur: "Β",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "bêta bas‐de‐casse",
				valeur: "β",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "bêta (variante)",
				valeur: "ϐ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "nu capital",
				valeur: "Ν",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "nu bas‐de‐casse",
				valeur: "ν",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "koppa capital (lettre archaïque)",
				valeur: "Ϙ",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "koppa capital (variante numérale)",
				valeur: "Ϟ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "koppa bas‐de‐casse (lettre archaïque)",
				valeur: "ϙ",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "koppa bas‐de‐casse (variante numérale)",
				valeur: "ϟ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "sampi capital (lettre archaïque)",
				valeur: "Ͳ",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "sampi capital (variante numérale)",
				valeur: "Ϡ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "sampi bas‐de‐casse (lettre archaïque)",
				valeur: "ͳ",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "sampi bas‐de‐casse (variante numérale)",
				valeur: "ϡ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "point d’interrogation",
				valeur: ";",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "signe numérique en pied",
				valeur: "͵",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "point",
				valeur: "·",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "signe numérique",
				valeur: "ʹ",
			},
		},
		{},
		{
			action: "majuscules",
			valeur: "Capitales",
			colspan: 2,
		},
	],
	[
		{
			valeur: "Contrôle",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Raccourci système",
		},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "mu bas‐de‐casse",
				valeur: "µ",
			},
			colspan: 7,
		},
		{
			action: "alternatives",
			valeur: "Alternative",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Menu contextuel",
		},
		{
			valeur: "Contrôle",
		},
	],
] as Array<Array<Touche>>
