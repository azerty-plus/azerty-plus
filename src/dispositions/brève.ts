import type { Touche } from "@/dispositions/index.ts"
import PositionTouche from "@/énumérations/PositionTouche.ts"

export default [
	[
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{
			valeur: "Retour en arrière",
		},
	],
	[
		{
			valeur: "Tabulation",
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "A brève",
				valeur: "Ă",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "a brève",
				valeur: "ă",
			},
		},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "E brève",
				valeur: "Ĕ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "e brève",
				valeur: "ĕ",
			},
		},
		{},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "U brève",
				valeur: "Ŭ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "u brève",
				valeur: "ŭ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "I brève",
				valeur: "Ĭ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "i barre",
				valeur: "ĭ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "O brève",
				valeur: "Ŏ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "o brève",
				valeur: "ŏ",
			},
		},
		{},
		{},
		{
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "diacritique brève",
				valeur: "◌̆",
				action: "général",
			},
		},
		{
			valeur: "Entrée",
			rowspan: 2,
		},
	],
	[
		{
			action: "majuscules",
			toucheRémanente: true,
			valeur: "Verrouiller capitales",
		},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
	],
	[
		{
			action: "majuscules",
			valeur: "Capitales",
		},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{
			action: "majuscules",
			valeur: "Capitales",
			colspan: 2,
		},
	],
	[
		{
			valeur: "Contrôle",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Raccourci système",
		},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "lettre brève",
				valeur: "˘",
			},
			colspan: 7,
		},
		{
			action: "alternatives",
			valeur: "Alternative",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Menu contextuel",
		},
		{
			valeur: "Contrôle",
		},
	],
] as Array<Array<Touche>>
