import type { Touche } from "@/dispositions/index.ts"
import PositionTouche from "@/énumérations/PositionTouche.ts"

export default [
	[
		{},
		{},
		{
			[PositionTouche.BasDroite]: {
				nouveau: false,
				titre: "diacritique tilde",
				valeur: "◌̃",
				action: "général",
			},
		},
		{},
		{},
		{},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "symbole de l’égalité asymptotique",
				valeur: "≃",
			},
		},
		{},
		{},
		{},
		{},
		{},
		{
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole de la presqu’inégalité",
				valeur: "≉",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "symbole de la presque égalité",
				valeur: "≈",
			},
		},
		{
			valeur: "Retour en arrière",
		},
	],
	[
		{
			valeur: "Tabulation",
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "A tilde",
				valeur: "Ã",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "a tilde",
				valeur: "ã",
			},
		},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "E tilde",
				valeur: "Ẽ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "e tilde",
				valeur: "ẽ",
			},
		},
		{},
		{},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "I tilde",
				valeur: "Ĩ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "i tilde",
				valeur: "ĩ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "O tilde",
				valeur: "Õ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "o tilde",
				valeur: "õ",
			},
		},
		{},
		{},
		{},
		{
			valeur: "Entrée",
			rowspan: 2,
		},
	],
	[
		{
			action: "majuscules",
			toucheRémanente: true,
			valeur: "Verrouiller capitales",
		},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
	],
	[
		{
			action: "majuscules",
			valeur: "Capitales",
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "supériorité ou équivalence à",
				valeur: "≳",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "non supériorité ou équivalence à",
				valeur: "≵",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "infériorité ou équivalence à",
				valeur: "≲",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "non infériorité ou équivalence à",
				valeur: "≴",
			},
		},
		{},
		{},
		{},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "N tilde",
				valeur: "Ñ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "n tilde",
				valeur: "ñ",
			},
		},
		{},
		{},
		{},
		{},
		{
			action: "majuscules",
			valeur: "Capitales",
			colspan: 2,
		},
	],
	[
		{
			valeur: "Contrôle",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Raccourci système",
		},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "lettre tilde",
				valeur: "~",
			},
			colspan: 7,
		},
		{
			action: "alternatives",
			valeur: "Alternative",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Menu contextuel",
		},
		{
			valeur: "Contrôle",
		},
	],
] as Array<Array<Touche>>
