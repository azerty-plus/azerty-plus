import type { Touche } from "@/dispositions/index.ts"
import PositionTouche from "@/énumérations/PositionTouche.ts"

export default [
	[
		{},
		{
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "diacritique combinant accent aigu",
				valeur: "◌́",
				action: "général",
			},
		},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{
			valeur: "Retour en arrière",
		},
	],
	[
		{
			valeur: "Tabulation",
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "A accent aigu",
				valeur: "Á",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "a accent aigu",
				valeur: "á",
			},
		},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "E accent aigu",
				valeur: "É",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "e accent aigu",
				valeur: "é",
			},
		},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "Y accent aigu",
				valeur: "Ý",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "y accent aigu",
				valeur: "ý",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "U accent aigu",
				valeur: "Ú",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "u accent aigu",
				valeur: "ú",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "I accent aigu",
				valeur: "Í",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "i accent aigu",
				valeur: "í",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "O accent aigu",
				valeur: "Ó",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "o accent aigu",
				valeur: "ó",
			},
		},
		{},
		{},
		{},
		{
			valeur: "Entrée",
			rowspan: 2,
		},
	],
	[
		{
			action: "majuscules",
			toucheRémanente: true,
			valeur: "Verrouiller capitales",
		},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
	],
	[
		{
			action: "majuscules",
			valeur: "Capitales",
		},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{
			action: "majuscules",
			valeur: "Capitales",
			colspan: 2,
		},
	],
	[
		{
			valeur: "Contrôle",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Raccourci système",
		},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "lettre accent aigu",
				valeur: "´",
			},
			colspan: 7,
		},
		{
			action: "alternatives",
			valeur: "Alternative",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Menu contextuel",
		},
		{
			valeur: "Contrôle",
		},
	],
] as Array<Array<Touche>>
