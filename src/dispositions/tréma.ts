import type { Touche } from "@/dispositions/index.ts"
import PositionTouche from "@/énumérations/PositionTouche.ts"

export default [
	[
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{
			valeur: "Retour en arrière",
		},
	],
	[
		{
			valeur: "Tabulation",
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "A tréma",
				valeur: "Ä",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "a tréma",
				valeur: "ä",
			},
		},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "E tréma",
				valeur: "Ë",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "e tréma",
				valeur: "ë",
			},
		},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "Y tréma",
				valeur: "Ÿ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "y tréma",
				valeur: "ÿ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "U tréma",
				valeur: "Ü",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "u tréma",
				valeur: "ü",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "I tréma",
				valeur: "Ï",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "double obèle",
				valeur: "‡",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "i tréma",
				valeur: "ï",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "O tréma",
				valeur: "Ö",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "o tréma",
				valeur: "ö",
			},
		},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "diacritique tréma",
				valeur: "◌̈",
				action: "général",
			},
		},
		{},
		{
			valeur: "Entrée",
			rowspan: 2,
		},
	],
	[
		{
			action: "majuscules",
			toucheRémanente: true,
			valeur: "Verrouiller capitales",
		},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
	],
	[
		{
			action: "majuscules",
			valeur: "Capitales",
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "symbole de négligibilité inverse par rapport à",
				valeur: "≫",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "symbole de négligibilité par rapport à",
				valeur: "≪",
			},
		},
		{},
		{},
		{},
		{},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "double point d’interrogation",
				valeur: "⁇",
			},
		},
		{},
		{},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "double point d’exclamation",
				valeur: "‼",
			},
		},
		{
			action: "majuscules",
			valeur: "Capitales",
			colspan: 2,
		},
	],
	[
		{
			valeur: "Contrôle",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Raccourci système",
		},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "lettre tréma",
				valeur: "¨",
			},
			colspan: 7,
		},
		{
			action: "alternatives",
			valeur: "Alternative",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Menu contextuel",
		},
		{
			valeur: "Contrôle",
		},
	],
] as Array<Array<Touche>>
