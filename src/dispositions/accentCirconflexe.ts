import type { Touche } from "@/dispositions/index.ts"
import PositionTouche from "@/énumérations/PositionTouche.ts"

export default [
	[
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "1 en exposant",
				valeur: "¹",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "1 en indice",
				valeur: "₁",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "2 en exposant",
				valeur: "²",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "2 en indice",
				valeur: "₂",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "3 en exposant",
				valeur: "³",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "3 en indice",
				valeur: "₃",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "4 en exposant",
				valeur: "⁴",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "4 en indice",
				valeur: "₄",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "5 en exposant",
				valeur: "⁵",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "5 en indice",
				valeur: "₅",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "parenthèse ouvrante en exposant",
				valeur: "⁽",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "parenthèse ouvrante en indice",
				valeur: "₍",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "6 en exposant",
				valeur: "⁶",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "6 en indice",
				valeur: "₆",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "symbole de la soustraction en exposant",
				valeur: "⁻",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "symbole de la soustraction en indice",
				valeur: "₋",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "7 en exposant",
				valeur: "⁷",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "7 en indice",
				valeur: "₇",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "8 en exposant",
				valeur: "⁸",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "8 en indice",
				valeur: "₈",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "9 en exposant",
				valeur: "⁹",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "9 en indice",
				valeur: "₉",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "0 en exposant",
				valeur: "⁰",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "0 en indice",
				valeur: "₀",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "opérateur cercle",
				valeur: "∘",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "parenthèse fermante en exposant",
				valeur: "⁾",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "parenthèse fermante en indice",
				valeur: "₎",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "plus en exposant",
				valeur: "⁺",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "plus en indice",
				valeur: "₊",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "symbole de l’égalité en exposant",
				valeur: "⁼",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "symbole de l’égalité en indice",
				valeur: "₌",
			},
		},
		{
			valeur: "Retour en arrière",
		},
	],
	[
		{
			valeur: "Tabulation",
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "A accent circonflexe",
				valeur: "Â",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "a accent circonflexe",
				valeur: "â",
			},
		},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "E accent circonflexe",
				valeur: "Ê",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "e accent circonflexe",
				valeur: "ê",
			},
		},
		{},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "U accent circonflexe",
				valeur: "Û",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "u accent circonflexe",
				valeur: "û",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "I accent circonflexe",
				valeur: "Î",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "i accent circonflexe",
				valeur: "î",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: false,
				titre: "O accent grave",
				valeur: "Ô",
			},
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "o accent circonflexe",
				valeur: "ô",
			},
		},
		{},
		{
			[PositionTouche.BasGauche]: {
				nouveau: false,
				titre: "diacritique accent circonflexe",
				valeur: "◌̂",
				action: "général",
			},
		},
		{},
		{
			valeur: "Entrée",
			rowspan: 2,
		},
	],
	[
		{
			action: "majuscules",
			toucheRémanente: true,
			valeur: "Verrouiller capitales",
		},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "S accent circonflexe",
				valeur: "Ŝ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "s accent circonflexe",
				valeur: "ŝ",
			},
		},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "G accent circonflexe",
				valeur: "Ĝ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "g accent circonflexe",
				valeur: "ĝ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "H accent circonflexe",
				valeur: "Ĥ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "h accent circonflexe",
				valeur: "ĥ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "J accent circonflexe",
				valeur: "Ĵ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "j accent circonflexe",
				valeur: "ĵ",
			},
		},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "i en exposant",
				valeur: "ⁱ",
			},
		},
		{},
		{},
		{},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "Astérisme",
				valeur: "⁂",
			},
		},
	],
	[
		{
			action: "majuscules",
			valeur: "Capitales",
		},
		{},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "C accent circonflexe",
				valeur: "Ĉ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "c accent circonflexe",
				valeur: "ĉ",
			},
		},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "n en exposant",
				valeur: "ⁿ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "n en indice",
				valeur: "ₙ",
			},
		},
		{},
		{},
		{},
		{},
		{
			action: "majuscules",
			valeur: "Capitales",
			colspan: 2,
		},
	],
	[
		{
			valeur: "Contrôle",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Raccourci système",
		},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "lettre accent circonflexe",
				valeur: "^",
			},
			colspan: 7,
		},
		{
			action: "alternatives",
			valeur: "Alternative",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Menu contextuel",
		},
		{
			valeur: "Contrôle",
		},
	],
] as Array<Array<Touche>>
