import type { Touche } from "@/dispositions/index.ts"
import PositionTouche from "@/énumérations/PositionTouche.ts"

export default [
	[
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{
			valeur: "Retour en arrière",
		},
	],
	[
		{
			valeur: "Tabulation",
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "A macron",
				valeur: "Ā",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "a macron",
				valeur: "ā",
			},
		},
		{
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "E macron",
				valeur: "Ē",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "e macron",
				valeur: "ē",
			},
		},
		{
		},
		{
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "Y macron",
				valeur: "Ȳ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "y macron",
				valeur: "ȳ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "U macron",
				valeur: "Ū",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "u macron",
				valeur: "ū",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "I macron",
				valeur: "Ī",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "i macron",
				valeur: "ī",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "O macron",
				valeur: "Ō",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "o macron",
				valeur: "ō",
			},
		},
		{
		},
		{
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "diacritique macron",
				valeur: "◌̄",
				action: "général",
			},
		},
		{
		},
		{
			valeur: "Entrée",
			rowspan: 2,
		},
	],
	[
		{
			action: "majuscules",
			toucheRémanente: true,
			valeur: "Verrouiller capitales",
		},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
	],
	[
		{
			action: "majuscules",
			valeur: "Capitales",
		},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{
			action: "majuscules",
			valeur: "Capitales",
			colspan: 2,
		},
	],
	[
		{
			valeur: "Contrôle",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Raccourci système",
		},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "lettre macron",
				valeur: "¯",
			},
			colspan: 7,
		},
		{
			action: "alternatives",
			valeur: "Alternative",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Menu contextuel",
		},
		{
			valeur: "Contrôle",
		},
	],
] as Array<Array<Touche>>
