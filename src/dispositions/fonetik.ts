import type { Touche } from "@/dispositions/index.ts"
import PositionTouche from "@/énumérations/PositionTouche.ts"

export default [
	[
		{},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "tirant suscrit combinant",
				valeur: "◌͡◌",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "tirant souscrit combinant",
				valeur: "◌͜◌",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "césure syllabique",
				valeur: ".",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "lettre tirant souscrit (liaison)",
				valeur: "‿",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "semi‐chrone",
				valeur: "ˑ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "chrone",
				valeur: "ː",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "brève combinant",
				valeur: "◌̆",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "accent tonique secondaire",
				valeur: "ˌ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "accent tonique primaire",
				valeur: "ˈ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "taquet haut souscrit combinant",
				valeur: "◌̝",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "signe plus souscrit combinant",
				valeur: "◌̟",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "taquet bas souscrit combinant",
				valeur: "◌̞",
				},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "signe moins souscrit combinant",
				valeur: "◌̠",
			},
		},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{
			valeur: "Retour en arrière",
		},
	],
	[
		{
			valeur: "Tabulation",
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "alpha bas‐de‐casse culbuté (latin)",
				valeur: "ɒ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "alpha bas‐de‐casse (latin)",
				valeur: "ɑ",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "a culbuté",
				valeur: "ɐ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "e ouvert culbuté",
				valeur: "ɜ",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "ɛ culbuté fermé",
				valeur: "ɞ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "e ouvert",
				valeur: "ɛ",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "e ouvert culbuté rhotique",
				valeur: "ɝ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "e inversé",
				valeur: "ɘ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "e culbuté (schwa)",
				valeur: "ə",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "e culbuté rhotique",
				valeur: "ɚ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "r petite capitale renversé",
				valeur: "ʁ",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "r petite capitale",
				valeur: "ʀ",
				},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "r culbuté",
				valeur: "ɹ",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "r sans obit",
				valeur: "ɾ",
			},
		},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "y petite capitale",
				valeur: "ʏ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "y culbuté",
				valeur: "ʎ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "upsilon bas‐de‐casse (latin)",
				valeur: "ʊ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "u barre inscrite",
				valeur: "ʉ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "i petite capitale",
				valeur: "ɪ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "i barre inscrite",
				valeur: "ɨ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "o barre oblique",
				valeur: "ø",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "o barre inscrite",
				valeur: "ɵ",
				},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "o ouvert à gauche",
				valeur: "ɔ",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "ligature o dans l’e petite capitale",
				valeur: "ɶ",
			},
		},
		{
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "symboles phonétiques",
				valeur: "ɸ",
				action: "général",
			},
		},
		{},
		{},
		{
			valeur: "Entrée",
			rowspan: 2,
		},
	],
	[
		{
			class: "permanente",
			valeur: "Verrouiller capitales",
			boutonVerrouillageCapitales: true,
		},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "thêta bas‐de‐casse",
				valeur: "θ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "eth bas‐de‐casse",
				valeur: "ð",
			},
		},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "phi bas‐de‐casse (latin)",
				valeur: "ɸ",
			},
		},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "petit gamma (latin)",
				valeur: "ɤ",
			},
		},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "h culbuté",
				valeur: "ɥ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "ezh bas‐de‐casse",
				valeur: "ʒ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "esh bas‐de‐casse",
				valeur: "ʃ",
			},
		},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "m hameçon",
				valeur: "ɱ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "m culbuté",
				valeur: "ɯ",
			},
		},
		{},
		{},
	],
	[
		{
			action: "majuscules",
			valeur: "Capitales",
		},
		{},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "w culbuté",
				valeur: "ʍ",
			},
		},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "khi bas‐de‐casse",
				valeur: "χ",
			},
		},
		{},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "v culbuté",
				valeur: "ʌ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "b petite capitale",
				valeur: "ʙ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "eng bas‐de‐casse",
				valeur: "ŋ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "n hameçon à gauche",
				valeur: "ɲ",
			},
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "coup de glotte (latin)",
				valeur: "ʔ",
			},
		},
		{},
		{},
		{},
		{
			action: "majuscules",
			valeur: "Capitales",
			colspan: 2,
		},
	],
	[
		{
			valeur: "Contrôle",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Raccourci système",
		},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "phi bas‐de‐casse (latin)",
				valeur: "ɸ",
			},
			colspan: 7,
		},
		{
			action: "alternatives",
			valeur: "Alternative",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Menu contextuel",
		},
		{
			valeur: "Contrôle",
		},
	],
] as Array<Array<Touche>>
