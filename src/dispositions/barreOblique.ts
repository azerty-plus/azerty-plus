import type { Touche } from "@/dispositions/index.ts"
import PositionTouche from "@/énumérations/PositionTouche.ts"

export default [
	[
		{},
		{},
		{},
		{
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole « Il n’existe pas »",
				valeur: "∄",
			},
		},
		{},
		{},
		{
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "barre oblique",
				valeur: "/",
				action: "général",
			},
		},
		{},
		{},
		{},
		{},
		{},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "symbole de l’inégalité",
				valeur: "≠",
			},
		},
		{
			valeur: "Retour en arrière",
		},
	],
	[
		{
			valeur: "Tabulation",
		},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "symbole de l’ensemble vide",
				valeur: "∅",
			},
		},
		{},
		{},
		{},
		{
			valeur: "Entrée",
			rowspan: 2,
		},
	],
	[
		{
			action: "majuscules",
			toucheRémanente: true,
			valeur: "Verrouiller capitales",
		},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "K barré (« Ker »)",
				valeur: "Ꝃ",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "k barré (« Ker »)",
				valeur: "ꝃ",
			},
		},
		{},
		{},
		{},
		{},
	],
	[
		{
			action: "majuscules",
			valeur: "Capitales",
		},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "symbole de non supériorité",
				valeur: "≯",
			},
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole de non supériorité ou égalité",
				valeur: "≱",
			},
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "symbole de non infériorité",
				valeur: "≮",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "symbole de non infériorité ou égalité",
				valeur: "≰",
			},
		},
		{
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole de non inclusion",
				valeur: "⊄",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "symbole de non appartenance",
				valeur: "∉",
			},
		},
		{},
		{},
		{
			[PositionTouche.HautDroite]: {
				nouveau: true,
				titre: "symbole de non implication",
				valeur: "⇏",
			},
			[PositionTouche.BasDroite]: {
				nouveau: true,
				titre: "symbole de non équivalence ou de disjonction exclusive",
				valeur: "⇎",
			},
		},
		{},
		{},
		{},
		{},
		{
			[PositionTouche.HautGauche]: {
				nouveau: true,
				titre: "barre oblique",
				valeur: "/",
			},
		},
		{},
		{
			action: "majuscules",
			valeur: "Capitales",
			colspan: 2,
		},
	],
	[
		{
			valeur: "Contrôle",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Raccourci système",
		},
		{
			[PositionTouche.BasGauche]: {
				nouveau: true,
				titre: "barre oblique",
				valeur: "/",
			},
			colspan: 7,
		},
		{
			action: "alternatives",
			valeur: "Alternative",
		},
		{
			valeur: "Système",
		},
		{
			valeur: "Menu contextuel",
		},
		{
			valeur: "Contrôle",
		},
	],
] as Array<Array<Touche>>
