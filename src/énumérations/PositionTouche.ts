enum PositionTouche {
	HautGauche = "hautGauche",
	HautDroite = "hautDroite",
	BasGauche = "basGauche",
	BasDroite = "basDroite",
}

export default PositionTouche
