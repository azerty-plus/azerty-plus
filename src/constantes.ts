import PositionTouche from "@/énumérations/PositionTouche.ts"

export const COULEURS = {
	[PositionTouche.BasGauche]: "bg-white",
	[PositionTouche.HautGauche]: "bg-red-200",
	[PositionTouche.BasDroite]: "bg-green-200",
	[PositionTouche.HautDroite]: "bg-blue-200",
}
